import QtQuick 2.11



Rectangle {
    id: popUpDecide
    signal tirar_clicked
    signal pasar_clicked



    width: 250
    height: 38
    color: "#80ffffff"
    radius: 2
    border.width: 0

    Rectangle {
        id: rectangle
        x: 8
        y: 8
        width: 70
        height: 28
        color: "#3d4dc2"
        radius: 4
        anchors.verticalCenter: parent.verticalCenter
        border.width: 0

        Text {
            id: element1
            x: 46
            y: 12
            color: "#ffffff"
            text: "Tirar"
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: 12
        }

        MouseArea {
            id: mouseAreatirar
            anchors.fill: parent
            onClicked: {
                           popUpDecide.tirar_clicked();
                       }
        }
    }


    Rectangle {
        id: rectangle1
        x: 172
        y: 8
        width: 70
        height: 28
        color: "#ee3f3f"
        radius: 4
        anchors.verticalCenter: parent.verticalCenter
        border.width: 0

        Text {
            id: element2
            x: 42
            y: 12
            color: "#ffffff"
            text: qsTr("Pasar")
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter: parent.verticalCenter
            font.pixelSize: 12
        }

        MouseArea {
            id: mouseArea1
            anchors.fill: parent
            onClicked: {
                           popUpDecide.pasar_clicked();
                       }
        }
    }


}









/*##^## Designer {
    D{i:3;anchors_height:100;anchors_width:100}D{i:6;anchors_height:100;anchors_width:100}
}
 ##^##*/

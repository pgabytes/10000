function reinicioJuego()
{
    reiniciacontadores()
    elementosmodel=0  //cantidad de elementos del modelo de datos
    proxmodelo= 0     //forma el modelo de datos que tiene que mostrar siempre que siga jugando

    jugador= 0
    puntos= 0
    puntospartida= 0 //este guarda los puntos obtenidos durante la partida

    arrayDados= []
    arrayDadosRuntime= []

    element.text="0"
    element1.text="0"
    element2.text="0"
    element3.text="0"
    text_pasar.text="Pasar"
    text_tirar.text="Tirar"
    element4.text="NO HAY JUEGO"

    elementturno1.visible=true
    elementturno2.visible=false
    elementturno3.visible=false
    elementturno4.visible=false
}


function reiniciacontadores()
{
    sinjuego=0
    countuno=0
    countdos=0
    counttres=0
    countcuatro=0
    countcinco=0
    countseis=0
    puntos=0
}

function cuentalosdadosdelarray()
{
    console.log("cuento los valores de los dados del array")
    reiniciacontadores()
    for (var i=0;i<arrayDadosRuntime.length;i++)
    {
        console.log(arrayDadosRuntime[i])
        switch(arrayDadosRuntime[i])
        {
        case 1:
            countuno++
            break
        case 2:
            countdos++
            break
        case 3:
            counttres++
            break
        case 4:
            countcuatro++
            break
        case 5:
            countcinco++
            break
        case 6:
            countseis++
            break
        }
    }
}
function preparopuntosjugador()
{
    switch(jugador){
    case 1:
        puntos=parseInt(element.text)
        break
    case 2:
        puntos=parseInt(element1.text)
        break
    case 3:
        puntos=parseInt(element2.text)
        break
    case 4:
        puntos=parseInt(element3.text)
        break
    }
}

function suma()
{
    console.log("-----SUMA-----")
    //uno
    if (countuno>0 && countuno<3)
    {
        puntos=puntos+(countuno*100)
    }

    if (countuno>=3)
    {
        countuno=countuno-3
        puntos=puntos+(countuno*100)+1000
    }
    //dos
    if (countdos>=3)
    {
        countdos=countdos-3
        puntos=puntos+(countdos*100)+200
    }

    //tres
    if (counttres>=3)
    {
        counttres=counttres-3
        puntos=puntos+(counttres*100)+300
    }

    //cuatro
    if (countcuatro>=3)
    {
        countcuatro=countcuatro-3
        puntos=puntos+(countcuatro*100)+400
    }

    //cinco
    if (countcinco>0 && countcinco<3)
    {
        puntos=puntos+(countcinco*50)
    }
    if (countcinco>=3)
    {
        countcinco=countcinco-3
        puntos=puntos+(countcinco*100)+500
    }
    //seis
    if (countseis>=3)
    {
        countseis=countseis-3
        puntos=puntos+(countseis*100)+600
    }

    if (countuno===1 && countdos===1 && counttres===1 && countcuatro===1 && countcinco===1 && countseis===1)
    {
        puntos=1500
    }

    elementptotmp.text=puntos.toString()
    if (puntos>0){
        text_pasar.text="Sumar y pasar"
        text_tirar.text="Sumar y tirar"
    }else{
        text_pasar.text="Pasar"
        text_tirar.text="Tirar"
    }

    console.log("puntos  "+puntos.toString())

    if (puntos===0) //sin puntos no hay juego pasa al otro jugador
    {
        proxmodelo=0
        console.log("no hay juego, paso al jugador  "+jugador)
        elementptotmp.visible=false
    }else{
        console.log("paso puntos a elementptotmp")
        elementptotmp.visible=true
        elementptotmp.text=puntos.toString()
    }

    console.log("-----------")

}

function controlpuntos(parametro,puntaje)
{
    console.log("control puntos")
    console.log(parametro)
    switch (parametro)
    {
    case 1:
        element.text=parseInt(element.text)+puntaje
        finJuego(parseInt(element.text),1)
        break
    case 2:
        element1.text=parseInt(element1.text)+puntaje
        finJuego(parseInt(element1.text),2)
        break
    case 3:
        element2.text=parseInt(element2.text)+puntaje
        finJuego(parseInt(element2.text),3)
        break
    case 4:
        element3.text=parseInt(element3.text)+puntaje
        finJuego(parseInt(element3.text),4)
        break
    }

    console.log(puntaje)
    audiopuntos.play()
    puntospartida=puntospartida+puntaje
}

function finJuego(val,jugador){
    if (val===parseInt(comboBoxjuegahasta.currentText))
    {
        console.log("ganaste")
        element4.text="GANASTE"
        finjuego=true
        avisonojuego.visible=true
    }
    if (val>parseInt(comboBoxjuegahasta.currentText))
    {
        valorrestar=val-parseInt(comboBoxjuegahasta.currentText)

        element4.text="TE PASASTE, SE TE RESTARAN "+valorrestar.toString()
        avisonojuego.visible=true

        console.log("valor antes de restar")
        console.log(val)

        val=parseInt(comboBoxjuegahasta.currentText)-valorrestar

        console.log(val)
        console.log("valor despues de restar")

        switch (jugador)
        {
        case 1:
            element.text=val
            break
        case 2:
            element1.text=val
            break
        case 3:
            element2.text=val
            break
        case 4:
            element3.text=val
            break
        }        
    }
}

function restopuntaje_nojuego(nrojugador)
{
    switch (nrojugador)
    {
    case 1:
        element.text=parseInt(element.text)-puntospartida
        break
    case 2:
        element1.text=parseInt(element1.text)-puntospartida
        break
    case 3:
        element2.text=parseInt(element2.text)-puntospartida
        break
    case 4:
        element3.text=parseInt(element3.text)-puntospartida
        break
    }
    puntospartida=0
}

function nojuega()
{
    elementptotmp.visible=false //oculto el puntaje temporal
    if (puntos>0 && sinjuego===0){ //si quedaron puntos por asignar los paso
         controlpuntos(jugador,puntos)
    }
    reiniciacontadores()
    elementosmodel=0
    proxmodelo=6

    cambiojugador()
    puntospartida=0
    console.log("jugador funcion no juega  "+jugador.toString())
}

function jugadoresvisibles(cantidadjugadores)
{
    switch(cantidadjugadores){
    case 2:
        rectangle.visible=true
        rectangle1.visible=true
        rectangle2.visible=false
        rectangle3.visible=false
        break
    case 3:
        rectangle.visible=true
        rectangle1.visible=true
        rectangle2.visible=true
        rectangle3.visible=false
        break
    case 4:
        rectangle.visible=true
        rectangle1.visible=true
        rectangle2.visible=true
        rectangle3.visible=true
        break
    }
}

function cambiojugador()
{
    console.log("funcion cambio de jugador")
    jugador++       //cambio de jugador
    if (jugador>parseInt(comboBox.currentText))
    {
        jugador=1
    }
    switch(jugador)
    {
    case 1:
        window.color="#570000"
        elementturno1.visible=true
        elementturno2.visible=false
        elementturno3.visible=false
        elementturno4.visible=false
        myclass.hablar(elementturno1.text)
        break
    case 2:
        window.color="#063000"
        elementturno1.visible=false
        elementturno2.visible=true
        elementturno3.visible=false
        elementturno4.visible=false
        myclass.hablar(elementturno2.text)
        break
    case 3:
        window.color="#03002b"
        elementturno1.visible=false
        elementturno2.visible=false
        elementturno3.visible=true
        elementturno4.visible=false
        myclass.hablar(elementturno3.text)
        break
    case 4:
        window.color="#363201"
        elementturno1.visible=false
        elementturno2.visible=false
        elementturno3.visible=false
        elementturno4.visible=true
        myclass.hablar(elementturno4.text)
        break
    }
    text_pasar.text="Pasar"
    text_tirar.text="Tirar"
    preparopuntosjugador()
}

function juega()
{
    elementptotmp.visible=false
    elementosmodel=0
    if (proxmodelo===elementosmodel)
    {
        reiniciacontadores()
        elementosmodel=6
        proxmodelo=6
        cambiojugador()
        puntospartida=0
        console.log("jugador   "+jugador.toString())
    }else{
        console.log("proximo modelo "+proxmodelo.toString())
        console.log("funcion juega elementosmodel antes  "+elementosmodel.toString())
        elementosmodel=proxmodelo
        controlpuntos(jugador,puntos)
        console.log("funcion juega elementosmodel despues  "+elementosmodel.toString())
    }
    cuentalosdadosdelarray()

    if (controljuego()===false)
    {
        proxmodelo=0
        restopuntaje_nojuego(jugador)
        console.log("no hay juego, paso al jugador  "+jugador)
        controlpad.visible=false
        avisonojuego.visible=true
    }else{
        console.log("hay juego true")
    }

}

function escalera(dados)
{
    dados.sort()
    console.log(dados)
    escaleratmp=dados;
    if (JSON.stringify(dados)===JSON.stringify(escaleraseis)) //1,2,3,4,5,6
    {
        return true
    }
    else if (JSON.stringify(dados)===JSON.stringify(escaleraseis2)) //1,1,2,3,4,5
    {
        return true
    }
    else if (JSON.stringify(dados)===JSON.stringify(escaleraseis3)) //1,2,3,4,5,5
    {
        return true
    }
        else
        {
            escaleratmp.pop()
            if (JSON.stringify(dados)===JSON.stringify(escaleracinco1)) //2,3,4,5,6
            {
                return true
            }else{
                escaleratmp=dados
                escaleratmp.shift()
                if (JSON.stringify(dados)===JSON.stringify(escaleracinco2))
                {
                    return true
                }
            }
        }
}

function clicDado(indice,valDado)
{        
    console.log("proxmodelo antes "+proxmodelo)
    console.log("valor dado seleccionado "+ valDado.toString())

    //aca tengo que controlar si con ese valor puedo hacer una jugada

    arrayDadosRuntime.sort()
    console.log(arrayDadosRuntime.toString())

    var cuentaunos=0
    var cuentados=0
    var cuentatres=0
    var cuentacuatros=0
    var cuentacincos=0
    var cuentaseis=0
    var puntos=0

    //cuento cuantos dados de cada numero tengo

    for (var a=0;a<arrayDadosRuntime.length;a++)
    {
        switch(arrayDadosRuntime[a])
        {
        case 1:
            cuentaunos++
            break
        case 2:
            cuentados++
            break
        case 3:
            cuentatres++
            break
        case 4:
            cuentacuatros++
            break
        case 5:
            cuentacincos++
            break
        case 6:
            cuentaseis++
            break
        }
    }

    //averiguo si tengo escalera

    if(cuentaunos===1 && cuentados===1 && cuentatres===1 && cuentacuatros===1 && cuentacincos===1 && cuentaseis===1)
    {
        console.log("escalera de 6")
        puntos=1500
    }


    //_______________________________________________________________
    //si no tengo dados valor 1 o 5
    //averiguo cuantos tengo de cada uno
    var cuentaocurrencia=0
    if (valDado!==1||valDado!==5)
    {
        console.log("busco concurrencia")
        for (var x=0;x<arrayDadosRuntime.length;x++)
        {
            if (arrayDadosRuntime[x]===valDado)
            {
                cuentaocurrencia++
            }
        }
    }
    console.log("valor dado antes de preguntar bla bla "+valDado)
    console.log("valor concurrencia   "+cuentaocurrencia)

    if (cuentaocurrencia>2 || valDado===1 || valDado===5 || (cuentaunos===1 && cuentados===1 && cuentatres===1 && cuentacuatros===1 && cuentacincos===1 && cuentaseis===1))
    {

        console.log("$$$$$$$$$  valor dentro del juego $$$$$$$$$")
        function isItemInArray(array, item) {
            for (var i = 0; i < array.length; i++) {
                // This if statement depends on the format of your array
                if (array[i][0] === item[0] && array[i][1] === item[1]) {
                    console.log("devuelvo dado "+proxmodelo.toString())
                    proxmodelo++
                    arrayDados.splice(i, 1);
                    return true;   // Found it
                }
            }
            return false;   // Not found
        }


        // Then
        if (!isItemInArray(arrayDados, [indice, valDado])) {
            console.log("reservo dado "+proxmodelo.toString())
            proxmodelo--
            arrayDados.push([indice, valDado]);
        }else{
            console.log("esta")
        }

        console.log(arrayDados)
        console.log("cantidad de dados seleccionados: "+arrayDados.length.toString())
        console.log("proxmodelo  "+proxmodelo.toString())


        reiniciacontadores()
        console.log("arraydados  "+arrayDados.toString())
        for(var i = 0; i < arrayDados.length; ++i){
            if(arrayDados[i][1] === 1){
                countuno++;
            }
            if(arrayDados[i][1] === 2){
                countdos++;
            }
            if(arrayDados[i][1] === 3){
                counttres++;
            }
            if(arrayDados[i][1] === 4){
                countcuatro++;
            }
            if(arrayDados[i][1] === 5){
                countcinco++;
            }
            if(arrayDados[i][1] === 6){
                countseis++;
            }
        }
        suma() //suma manualmente para ver si va formando juego o no
        console.log("clic dado llama suma()")
        return true
    }else{
        audionosele.play()
        sinjuego++
        if (sinjuego>2)
        {
            console.log("no se puede seleccionar "+sinjuego.toString())
            proxmodelo=0
            puntos=0

            element4.text="JUEGA CORRECTAMENTE!"
            avisonojuego.visible=true
        }

        console.log("no se puede seleccionar")
        return false
    }
}

function controljuego() //controla si hay o no juego al tirar
{
    console.log("-----------")
    console.log("controla si hay juego")

    //uno
    if (countuno >0 && countuno <3)
    {
        console.log("-1-")
        return true
    }
    if (countuno>=3){
        console.log("-2-")
        return true
    }
    if (countdos>=3){
        console.log("-3-")
        return true
    }
    if (counttres>=3){
        console.log("-4-")
        return true
    }
    if (countcuatro>=3){
        console.log("-5-")
        return true
    }
    if ( countcinco>0 && countcinco<3){
        console.log("-6-")
        return true
    }
    if (countcinco>=3){
        console.log("-7-")
        return true
    }
    if (countseis>=3){
        console.log("-8-")
        return true
    }
    if (countuno===1 && countdos===1 && counttres===1 && countcuatro===1 && countcinco===1 && countseis===1)
    {
        console.log("escalera total 1500 puntos")
        return true
    }    
    //reiniciacontadores()
    return false
}

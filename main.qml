import QtQuick 2.11
import QtQuick.Window 2.11
import QtQuick.Controls 2.2
import QtMultimedia 5.9
import QtQuick.Controls.Material 2.2
import foo 1.0    // Import this module, so we can use it in our QML script

import "lasfunciones.js" as Logic

Window {

    id: window
    visible: true
    width: 300
    height: 480
    color: "#000000"

    title: comboBox.currentText+"/"+comboBoxjuegahasta.currentText

    property int countuno:0
    property int countdos:0
    property int counttres:0
    property int countcuatro:0
    property int countcinco:0
    property int countseis:0

    property int elementosmodel:0  //cantidad de elementos del modelo de datos
    property int proxmodelo: 0     //forma el modelo de datos que tiene que mostrar siempre que siga jugando

    property int jugador: 0
    property int puntos: 0
    property int puntospartida: 0 //este guarda los puntos obtenidos durante la partida

    property variant arrayDados: []
    property variant arrayDadosRuntime: []

    property bool finjuego: false
    property int sinjuego:0 //suma la cantidad de veces que toco dados sin valor
    property int valorrestar: 0 //cuando me paso del valor resto sobre el puntaje del jugador
    property int gamernumber: 0 //nro de jugador para referenciar el nombre
    property int estadojuego: 0

    QmlCppBridge{
        id: myclass
    }


    Image {
        id: image
        anchors.fill: parent
        source: "fondo.jpg"
        fillMode: Image.Stretch
    }

    Audio{
        id:audiotirar
        source: "MANYDICE.wav"
    }

    Audio{
        id:audiopuntos
        source: "puntos.wav"
    }

    Audio{
        id:audioon
        source: "on.wav"
    }
    Audio{
        id:audionosele //audio para el dado sin valor
        source: "error.wav"
    }

    Grid{
        id:grillaventana
        anchors.top: parent.top
        anchors.topMargin: 39
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 44
        anchors.left: parent.left
        anchors.leftMargin: 5
        anchors.right: parent.right
        anchors.rightMargin: 5
        clip: false
        spacing: 2
        rows: 3
        columns: 2

        Repeater {
            id:modelo
            anchors.fill: parent
            //clip: true
            model: elementosmodel
            delegate: {
                elcuadrado
            }
        }

    }

    Rectangle {
        id: rectanglejugadores
        height: 33
        color: "#80ffffff"
        visible: false
        anchors.top: parent.top
        anchors.topMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 0

        Row {
            id: columnJugadores
            visible: true
            anchors.fill: parent
            spacing: 1


            Rectangle {
                id: rectangle
                width: parent.width/parseInt(comboBox.currentText)
                height: 20
                color: "#99f20000"
                radius: 5
                visible: false
                border.width: 0
                border.color: "#950606"

                Text {
                    id: element
                    color: "#ffffff"
                    text: qsTr("0")
                    anchors.fill: parent
                    font.bold: true
                    font.pixelSize: 18
                    lineHeight: 1
                    elide: Text.ElideMiddle
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                }

                Text {
                    id: elementturno1
                    x: 21
                    y: 18
                    color: "#000000"
                    text: "Tu Turno"
                    font.bold: true
                    visible: true
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.pixelSize: 10
                }

                MouseArea {
                    id: mouseAreaT1
                    anchors.fill: parent
                    onClicked: {
                        gamernumber=1
                        nombrejugador.visible=true
                        textInput.focus=true
                    }
                }

            }

            Rectangle {
                id: rectangle1
                width: parent.width/parseInt(comboBox.currentText)
                height: 20
                color: "#991aee33"
                radius: 5
                visible: false
                border.width: 0
                rotation: 0

                Text {
                    id: element1
                    color: "#ffffff"
                    text: qsTr("0")
                    anchors.fill: parent
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    font.bold: true
                    lineHeight: 1
                    elide: Text.ElideMiddle
                    font.pixelSize: 18
                }

                Text {
                    id: elementturno2
                    x: 21
                    y: 18
                    color: "#000000"
                    text: "Tu Turno"
                    font.bold: true
                    visible: false
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.pixelSize: 10
                }

                MouseArea {
                    id: mouseAreaT2
                    anchors.fill: parent
                    onClicked: {
                        gamernumber=2
                        nombrejugador.visible=true
                    }
                }

                border.color: "#054308"
            }

            Rectangle {
                id: rectangle2
                width: parent.width/parseInt(comboBox.currentText)
                height: 20
                color: "#991b56ee"
                radius: 5
                border.width: 0
                rotation: 0

                Text {
                    id: element2
                    color: "#ffffff"
                    text: qsTr("0")
                    anchors.fill: parent
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    font.bold: true
                    lineHeight: 1
                    font.pixelSize: 18
                    elide: Text.ElideMiddle
                }

                Text {
                    id: elementturno3
                    x: 21
                    y: 18
                    color: "#000000"
                    text: "Tu Turno"
                    font.bold: true
                    visible: false
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.pixelSize: 10
                }
                MouseArea {
                    id: mouseAreaT3
                    anchors.fill: parent
                    onClicked: {
                        gamernumber=3
                        nombrejugador.visible=true
                    }
                }

                border.color: "#0c1881"
            }

            Rectangle {
                id: rectangle3
                width: parent.width/parseInt(comboBox.currentText)
                height: 20
                color: "#99c5db0d"
                radius: 5
                border.width: 0
                rotation: 0

                Text {
                    id: element3
                    color: "#ffffff"
                    text: qsTr("0")
                    anchors.fill: parent
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    font.bold: true
                    lineHeight: 1
                    elide: Text.ElideMiddle
                    font.pixelSize: 18
                }

                Text {
                    id: elementturno4
                    x: 21
                    y: 18
                    color: "#000000"
                    text: "Tu Turno"
                    font.bold: true
                    visible: false
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.pixelSize: 10
                }
                MouseArea {
                    id: mouseAreaT4
                    anchors.fill: parent
                    onClicked: {
                        gamernumber=4
                        nombrejugador.visible=true
                    }
                }

                border.color: "#6c5b0a"
            }
        }
    }


    Rectangle{
        id:controlpad
        height: 38
        color: "#80ffffff"
        radius: 2
        anchors.left: parent.left
        anchors.leftMargin: 2
        anchors.right: parent.right
        anchors.rightMargin: 2
        visible: false
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        border.width: 0

        Rectangle {
            y: 8
            width: 100
            height: 28
            color: "#3d4dc2"
            radius: 8
            anchors.left: parent.left
            anchors.leftMargin: 5
            anchors.verticalCenter: parent.verticalCenter
            border.width: 0

            Text {
                id:text_tirar
                x: 46
                y: 12
                color: "#ffffff"
                text: "Tirar"
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: 12
            }

            MouseArea {
                id: mouseAreatirar
                anchors.fill: parent
                onClicked: {
                    console.log("tirar")
                    console.log(proxmodelo)
                    if (estadojuego===0)
                    {
                        arrayDadosRuntime=[]
                        audiotirar.play()
                        Logic.juega()
                        Logic.reiniciacontadores()
                        console.log("jugando")
                        estadojuego=1
                    }
                    if (estadojuego===1 && proxmodelo<6)
                    {
                        arrayDadosRuntime=[]
                        audiotirar.play()
                        Logic.juega()
                        Logic.reiniciacontadores()
                        console.log("jugando")
                    }
                }
            }
        }


        Rectangle {
            x: 191
            y: 8
            width: 100
            height: 28
            color: "#ee3f3f"
            radius: 8
            anchors.right: parent.right
            anchors.rightMargin: 5
            anchors.verticalCenterOffset: 0
            anchors.verticalCenter: parent.verticalCenter
            border.width: 0

            Text {
                id:text_pasar
                x: 42
                y: 12
                color: "#ffffff"
                text: qsTr("Pasar")
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                font.pixelSize: 12
            }

            MouseArea {
                id: mouseAreaPasar
                anchors.fill: parent
                onClicked: {
                    console.log("pasar y tirar")
                    console.log(proxmodelo)
                    if (proxmodelo<6)
                    {
                        console.log("pasar y tirar se cumple condicion")
                        arrayDadosRuntime=[]
                        Logic.nojuega()
                        elementosmodel=0
                        Logic.reiniciacontadores()
                        estadojuego=0
                    }
                }
            }
        }

        Text {
            id: elementptotmp
            x: 113
            y: 11
            color: "#cc0e0e0e"
            text: "0"
            visible: false
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: 12
        }
    }

    Component{
        id:elcuadrado
        Rectangle {
            property var colorsi:"#e8e8e8"
            property var colorsm:"#e8e8e8"
            property var colorsd:"#e8e8e8"
            property var colormi:"#e8e8e8"
            property var colormm:"#e8e8e8"
            property var colormd:"#e8e8e8"
            property var colorii:"#e8e8e8"
            property var colorim:"#e8e8e8"
            property var colorid:"#e8e8e8"
            property int valorDado
            property bool isBlanco: true

            radius: 10
            width: grillaventana.width/2
            height: grillaventana.height/3
            color: "#e8e8e8"
            border.color: isBlanco ? "#9c9898" : "#eb4034"
            border.width: 10
            Component.onCompleted: {
                valorDado=sortea()
                arrayDadosRuntime.push(valorDado)
                console.log(arrayDadosRuntime)
                console.log("valorDado   "+valorDado.toString())
            }
            MouseArea{
                id:mouseAreaDado
                anchors.fill: parent
                onClicked: {
                    console.log("clic en el dado antes de la funcion")
                    if(Logic.clicDado(index,valorDado))
                    {
                        parent.isBlanco = !parent.isBlanco
                        elementptotmp.visible=true
                    }else{
                        elementptotmp.visible=false
                    }

                    console.log("clic en el dado despues de la funcion")
                }
            }


            Grid{
                anchors.centerIn: parent
                rows:3
                columns: 3
                spacing: 10
                Rectangle{radius: 10; width: 20; height: 20; color:colorsi}
                Rectangle{radius: 10; width: 20; height: 20; color:colorsm}
                Rectangle{radius: 10; width: 20; height: 20; color:colorsd}
                Rectangle{radius: 10; width: 20; height: 20; color:colormi}
                Rectangle{radius: 10; width: 20; height: 20; color:colormm}
                Rectangle{radius: 10; width: 20; height: 20; color:colormd}
                Rectangle{radius: 10; width: 20; height: 20; color:colorii}
                Rectangle{radius: 10; width: 20; height: 20; color:colorim}
                Rectangle{radius: 10; width: 20; height: 20; color:colorid}
            }

            function sortea()
            {
                arrayDados=[]
                var random = Math.floor(Math.random() * 6) + 1;
                switch (random)
                {
                case 1:
                    //countuno++;
                    colorsi = "#e8e8e8"
                    colorsm = "#e8e8e8"
                    colorsd = "#e8e8e8"
                    colormi = "#e8e8e8"
                    colormm = "black"
                    colormd = "#e8e8e8"
                    colorii = "#e8e8e8"
                    colorim = "#e8e8e8"
                    colorid = "#e8e8e8"
                    break;
                case 2:
                    //countdos++;
                    colorsi = "black"
                    colorsm = "#e8e8e8"
                    colorsd = "#e8e8e8"
                    colormi = "#e8e8e8"
                    colormm = "#e8e8e8"
                    colormd = "#e8e8e8"
                    colorii = "#e8e8e8"
                    colorim = "#e8e8e8"
                    colorid = "black"
                    break;
                case 3:
                    //counttres++;
                    colorsi = "black"
                    colorsm = "#e8e8e8"
                    colorsd = "#e8e8e8"
                    colormi = "#e8e8e8"
                    colormm = "black"
                    colormd = "#e8e8e8"
                    colorii = "#e8e8e8"
                    colorim = "#e8e8e8"
                    colorid = "black"
                    break;
                case 4:
                    //countcuatro++;
                    colorsi = "black"
                    colorsm = "#e8e8e8"
                    colorsd = "black"
                    colormi = "#e8e8e8"
                    colormm = "#e8e8e8"
                    colormd = "#e8e8e8"
                    colorii = "black"
                    colorim = "#e8e8e8"
                    colorid = "black"
                    break;
                case 5:
                    //countcinco++;
                    colorsi = "black"
                    colorsm = "#e8e8e8"
                    colorsd = "black"
                    colormi = "#e8e8e8"
                    colormm = "black"
                    colormd = "#e8e8e8"
                    colorii = "black"
                    colorim = "#e8e8e8"
                    colorid = "black"
                    break;
                case 6:
                    //countseis++;
                    colorsi = "black"
                    colorsm = "black"
                    colorsd = "black"
                    colormi = "#e8e8e8"
                    colormm = "#e8e8e8"
                    colormd = "#e8e8e8"
                    colorii = "black"
                    colorim = "black"
                    colorid = "black"
                    break;
                }
                return random
            }
        }

    }


    Rectangle {
        id: avisonojuego
        x: 50
        y: 219
        width: window.width-5
        height: 73
        color: "#80ffffff"
        radius: 10
        border.width: 0
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        visible: false

        Text {
            id: element4
            x: 111
            y: 8
            color: "#000000"
            text: "NO HAY JUEGO"
            anchors.horizontalCenterOffset: 0
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: 12
        }

        Rectangle {
            id: rectangle5
            x: 88
            y: 8
            width: 70
            height: 28
            color: "#ee3f3f"
            radius: 4
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenterOffset: 15
            Text {
                id: element5
                x: 42
                y: 12
                color: "#ffffff"
                text: qsTr("OK")
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                font.pixelSize: 12
            }

            MouseArea {
                id: mouseArea1
                anchors.fill: parent
                onClicked: {
                    avisonojuego.visible=false
                    if (finjuego===false)
                    {
                        arrayDadosRuntime=[]
                        //Logic.nojuega() //aca era llamado por segunda vez
                        elementosmodel=0
                        Logic.reiniciacontadores()
                    }else{
                        Logic.reinicioJuego()
                        finjuego=false
                    }
                    element4.text="NO HAY JUEGO"
                    controlpad.visible=true
                }
            }
            anchors.verticalCenter: parent.verticalCenter
            border.width: 0
        }
    }

    Rectangle {
        id: controlcomienzo
        x: 0
        y: 0
        width: 250
        height: 166
        color: "#80ffffff"
        radius: 10
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        ComboBox {
            id: comboBoxjuegahasta
            x: 57
            y: 75
            width: 100
            height: 34
            flat: true
            font.pointSize: 12
            font.bold: true
            model: ["10000", "9000", "8000", "7000","6000","5000","4000","3000","2000","1000","500"]
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.horizontalCenterOffset: 68
            anchors.verticalCenter: parent.verticalCenter
            anchors.verticalCenterOffset: -7
        }

        Rectangle {
            x: 20
            y: 8
            width: 70
            height: 50
            color: "#cc1d9f17"
            radius: 10
            anchors.verticalCenterOffset: 41
            Text {
                x: 46
                y: 12
                color: "#ffffff"
                text: "Jugar"
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                font.pixelSize: 12
            }

            MouseArea {
                id: mouseAreacomienzo
                anchors.fill: parent
                onClicked: {
                    audioon.play()
                    controlcomienzo.visible=false
                    rectanglejugadores.visible=true
                    controlpad.visible=true
                    Logic.jugadoresvisibles(parseInt(comboBox.currentText))
                }
            }
            anchors.verticalCenter: parent.verticalCenter
            border.width: 0
        }

        ComboBox {
            id: comboBox
            x: 55
            y: 68
            width: 100
            height: 34
            flat: true
            font.pointSize: 12
            font.bold: true
            anchors.verticalCenterOffset: -7
            anchors.horizontalCenterOffset: -67
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            model: ["2", "3", "4"]
        }

        Text {
            id: element6
            x: 97
            y: 36
            text: qsTr("Jugadores")
            anchors.horizontalCenterOffset: -77
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: 12
        }

        Text {
            id: element8
            x: 87
            y: 8
            text: qsTr("Juego de dados")
            anchors.horizontalCenterOffset: 0
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: 12
        }

        Rectangle {
            x: 161
            y: 15
            width: 70
            height: 50
            color: "#ccef3e3e"
            radius: 10
            anchors.verticalCenter: parent.verticalCenter
            anchors.verticalCenterOffset: 41
            Text {
                x: 46
                y: 12
                color: "#ffffff"
                text: qsTr("Salir")
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: 12
            }

            MouseArea {
                id: mouseAreasalir
                anchors.fill: parent
                onClicked: {
                    close()
                }
            }
            border.width: 0
        }

        Text {
            id: element7
            x: 106
            y: 36
            text: qsTr("Jugar hasta")
            font.pixelSize: 12
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.horizontalCenterOffset: 68
        }

        border.width: 0
    }

    Rectangle {
        id: nombrejugador
        x: 25
        y: 152
        width: 250
        height: 116
        color: "#80ffffff"
        radius: 10
        visible: false
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        border.width: 0

        TextField {
            id: textInput
            width: 234
            height: 30

            text: qsTr("")
            cursorVisible: true
            anchors.verticalCenterOffset: -38
            anchors.horizontalCenterOffset: 0
            horizontalAlignment: Text.AlignHCenter
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            font.pixelSize: 12
        }

        Rectangle {
            x: 90
            y: 8
            width: 70
            height: 50
            color: "#3d4dc2"
            radius: 10
            anchors.verticalCenter: parent.verticalCenter
            Text {
                x: 46
                y: 12
                color: "#ffffff"
                text: "Guardar"
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                font.pixelSize: 12
            }

            MouseArea {
                id: mouseAreanomjug
                anchors.fill: parent
                onClicked: {
                    switch (gamernumber)
                    {
                    case 1:
                        elementturno1.text=textInput.text.toUpperCase()
                        break
                    case 2:
                        elementturno2.text=textInput.text.toUpperCase()
                        break
                    case 3:
                        elementturno3.text=textInput.text.toUpperCase()
                        break
                    case 4:
                        elementturno4.text=textInput.text.toUpperCase()
                        break
                    }
                    nombrejugador.visible=false
                    textInput.text=""
                    gamernumber=0
                }
            }


            border.width: 0
            anchors.verticalCenterOffset: 19
        }

    }

}

































/*##^## Designer {
    D{i:1;anchors_x:0;anchors_y:0}D{i:2;anchors_height:402;anchors_width:289;anchors_x:6;anchors_y:34}
D{i:12;anchors_height:100;anchors_width:100}D{i:22;anchors_x:8}D{i:21;anchors_width:250}
D{i:26;anchors_x:8}D{i:29;anchors_width:45;anchors_x:126}D{i:25;anchors_width:250}
D{i:33;anchors_width:45;anchors_x:126}
}
 ##^##*/

#include "qmlcppbridge.h"
#include <QLoggingCategory>

QmlCppBridge::QmlCppBridge(QObject *parent) : QObject(parent)
{
    QLoggingCategory::setFilterRules(QStringLiteral("qt.speech.tts=true \n qt.speech.tts.*=true"));
    const auto engines = QTextToSpeech::availableEngines();
    for (const QString &engine : engines)
        qDebug()<<engine;
    engineSelected();
}
void QmlCppBridge::hablar(QString contenido)
{
    qDebug()<<contenido;
    QLocale idioma;

    //m_speech->setLocale(idioma.Spanish);
    m_speech->setRate(-0.3);
    m_speech->setPitch(-0.5);
    m_speech->setVolume(50);
    m_speech->say(contenido);
}

void QmlCppBridge::engineSelected()
{
    QString engineName = "default";
    delete m_speech;
    if (engineName == "default")
        m_speech = new QTextToSpeech(this);
    else
        m_speech = new QTextToSpeech(engineName, this);
}

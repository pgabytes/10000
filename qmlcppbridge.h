#ifndef QMLCPPBRIDGE_H
#define QMLCPPBRIDGE_H

#include <QObject>
#include <QDebug>
#include <QTextToSpeech>


class QmlCppBridge : public QObject
{
    Q_OBJECT
public:
    explicit QmlCppBridge(QObject *parent = nullptr);
    Q_INVOKABLE void hablar(QString contenido);
    Q_INVOKABLE void engineSelected();
private:
    QTextToSpeech *m_speech;
    QVector<QVoice> m_voices;

signals:

public slots:
};

#endif // QMLCPPBRIDGE_H
